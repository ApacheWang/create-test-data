package com.exceeddata.mqtt.publish;

import com.hryt.adas.evaluate.HMIProto;

import java.util.Scanner;
import java.util.logging.Logger;

public class HMISend {
    private final String message = "";
    private static final String HMI2TSP_Topic = "hryt/adas/evaluate/hmi2tsp";
    private static HMIProto.EvaluateData start;
    private static HMIProto.EvaluateData end;
    private static HMIProto.EvaluateData submit_evaluation;
    private static HMIProto.EvaluateData request_report;
    private static HMIProto.EvaluateData tag_the_moment;
    private static HMIProto.EvaluateData send_tag_the_moment_reason;
    private static HMIProto.EvaluateData.Builder builder;
    private static int count = 0;

    public HMISend() {
        builder = HMIProto.EvaluateData.newBuilder();
        builder.setTimeStamp(System.currentTimeMillis());
        builder.setMsgCout(count);
        builder.setCmd(1);
        builder.setComfort(0);
        builder.setMobility(0);
        builder.setSafety(0);
        builder.setUserPosition(6);
        builder.setPassengerNum1(2);
        builder.setPassengerNum2(2);
        builder.setPassengerNum3(2);
    }

    public static void main(String[] args) {
        new HMISend();
        while(true) {
            Scanner sc = new Scanner(System.in);
            int cmd = sc.nextInt();
            System.out.println(cmd);
            if(cmd == 1) {
                builder.setTimeStamp(System.currentTimeMillis() / 1000);
                start = builder.build();
                builder.setCmd(1);
                start = builder.build();
                Publish_Main.send(HMI2TSP_Topic, start.toByteArray());
                System.out.println(start.toString());
            } else if(cmd == 2) {
                builder.setTimeStamp(System.currentTimeMillis() / 1000);
                end = builder.build();
                builder.setCmd(2);
                end = builder.build();
                Publish_Main.send(HMI2TSP_Topic, end.toByteArray());
                System.out.println(end.toString());
            } else if(cmd == 3) {
                builder.setTimeStamp(System.currentTimeMillis() / 1000);
                builder.setCmd(3);
                builder.setComfort(99);
                builder.setMobility(99);
                builder.setSafety(99);
                builder.setMode(7);
                submit_evaluation = builder.build();
                Publish_Main.send(HMI2TSP_Topic, submit_evaluation.toByteArray());
                System.out.println(submit_evaluation.toString() + "\n" + builder.getMode());
            } else if(cmd == 4) {
                builder.setTimeStamp(System.currentTimeMillis() / 1000);
                builder.setCmd(4);
                request_report = builder.build();
                Publish_Main.send(HMI2TSP_Topic, request_report.toByteArray());
                System.out.println(request_report.toString());
            } else if(cmd == 5) {
                builder.setTimeStamp(System.currentTimeMillis() / 1000);
                builder.setCmd(5);
                tag_the_moment = builder.build();
                Publish_Main.send(HMI2TSP_Topic, tag_the_moment.toByteArray());
                System.out.println(tag_the_moment.toString());
            } else if(cmd == 6) {
                Scanner reason = new Scanner(System.in);
                int reason_num = reason.nextInt();
                builder.setTimeStamp(System.currentTimeMillis() / 1000);
                builder.setCmd(6);
                builder.setReason1(reason_num);
                send_tag_the_moment_reason = builder.build();
                Publish_Main.send(HMI2TSP_Topic, send_tag_the_moment_reason.toByteArray());
                System.out.println(send_tag_the_moment_reason.toString());
            } else {
                System.out.println("no cmd match ! ");
            }
        }
    }

}
