package com.exceeddata.mqtt.publish;

import com.hryt.adas.evaluate.HMIProto;
import org.eclipse.paho.client.mqttv3.MqttException;

import java.net.URISyntaxException;
import java.util.Scanner;

public class Main {
    private static final String URL = "tcp://master:1883";
    private static final String OBU2TSP = "hryttest";
    private final String message = "";
    private static final String HMI2TSP_Topic = "hryt/adas/evaluate/hmi2tsp";
    private static HMIProto.EvaluateData start;
    private static HMIProto.EvaluateData end;
    private static HMIProto.EvaluateData submit_evaluation;
    private static HMIProto.EvaluateData request_report;
    private static HMIProto.EvaluateData tag_the_moment;
    private static HMIProto.EvaluateData send_tag_the_moment_reason;
    private static HMIProto.EvaluateData.Builder builder;
    private static int count = 0;

    static {
        builder = HMIProto.EvaluateData.newBuilder();
        builder.setTimeStamp(System.currentTimeMillis());
        builder.setMsgCout(count);
        builder.setCmd(1);
        builder.setComfort(0);
        builder.setMobility(0);
        builder.setSafety(0);
        builder.setUserPosition(6);
        builder.setPassengerNum1(2);
        builder.setPassengerNum2(2);
        builder.setPassengerNum3(2);
    }

    public static void main(String[] args) throws MqttException, URISyntaxException {
        Publish publish = new Publish("OBU TO TSP", OBU2TSP);
        Thread publish_thread = new Thread(publish);
        publish_thread.start();

        while(true) {
            Scanner sc = new Scanner(System.in);
            int cmd = sc.nextInt();
            System.out.println(cmd);
            if(cmd == 1) {
                builder.setTimeStamp(System.currentTimeMillis());
                start = builder.build();
                builder.setCmd(1);
                Publish_Main.send(HMI2TSP_Topic, start.toByteArray());
            } else if(cmd == 2) {
                builder.setTimeStamp(System.currentTimeMillis());
                end = builder.build();
                builder.setCmd(2);
                Publish_Main.send(HMI2TSP_Topic, end.toByteArray());
            } else if(cmd == 3) {
                builder.setTimeStamp(System.currentTimeMillis());
                builder.setCmd(3);
                builder.setComfort(99);
                builder.setMobility(99);
                builder.setSafety(99);
                submit_evaluation = builder.build();
                Publish_Main.send(HMI2TSP_Topic, submit_evaluation.toByteArray());
            } else if(cmd == 4) {
                builder.setTimeStamp(System.currentTimeMillis());
                builder.setCmd(4);
                request_report = builder.build();
                Publish_Main.send(HMI2TSP_Topic, request_report.toByteArray());
            } else if(cmd == 5) {
                builder.setTimeStamp(System.currentTimeMillis());
                builder.setCmd(5);
                tag_the_moment = builder.build();
                Publish_Main.send(HMI2TSP_Topic, tag_the_moment.toByteArray());
            } else if(cmd == 6) {
                Scanner reason = new Scanner(System.in);
                int reason_num = sc.nextInt();
                builder.setTimeStamp(System.currentTimeMillis());
                builder.setReason1(3);
                send_tag_the_moment_reason = builder.build();
                Publish_Main.send(HMI2TSP_Topic, send_tag_the_moment_reason.toByteArray());
            } else {
                System.out.println("no cmd match ! ");
            }
        }
    }
}
