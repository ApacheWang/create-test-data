package com.exceeddata.mqtt.publish;

import hryt.common.HeaderOuterClass;
import hryt.obu.OBU2HMI;
import org.eclipse.paho.client.mqttv3.MqttException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Publish implements Runnable {
    private String topic;
    private String name;
    private OBU2HMI.VehiclePosition VehiclePosit;
    private static OBU2HMI.VehiclePosition.Builder builder = OBU2HMI.VehiclePosition.newBuilder();
    private HeaderOuterClass.Header header;
    private static HeaderOuterClass.Header.Builder builder_header = HeaderOuterClass.Header.newBuilder();
    List<List<String>> records = new ArrayList<List<String>>();


    public Publish(String name, String topic) {
        this.name = name;
        this.topic = topic;



        try {
            try (BufferedReader br = new BufferedReader(new FileReader("/root/VehiclePosition.csv"))) {
                String line;
                while ((line = br.readLine()) != null) {
                    String[] values = line.split(",");
                    records.add(Arrays.asList(values));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while(true) {
            for(int i = 0, size = records.size(); i < size; ++i) {
                builder.setId("car_028");
                builder.setLat(31.267100792);
                builder.setLng(121.616564105);
                builder.setSpeed(Double.parseDouble(records.get(i).get(1)));
                builder.setLngAcc(Double.parseDouble(records.get(i).get(2)));
                builder.setLatAcc(Double.parseDouble(records.get(i).get(3)));
                builder.setYawRate(Double.parseDouble(records.get(i).get(4)));

                builder_header.setTimestamp(Long.parseLong(records.get(i).get(0)));
                builder_header.setMsgType(5);
                header = builder_header.build();

                builder.setHeader(header);
                VehiclePosit = builder.build();
                Publish_Main.send(topic, VehiclePosit.toByteArray());
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
