package com.exceeddata.mqtt.publish;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class Publish_Main {
	private static boolean flag = true;
	private static MqttClient sampleClient;
	private static int qos = 0;
	private static String topic;
	private static String broker = "tcp://master:1883";

	Publish_Main() {
//		init();
	}

	public static void init() {
		String clientId = "pubClient";
		MemoryPersistence persistence = new MemoryPersistence();
		try {
			sampleClient = new MqttClient(broker, clientId, persistence);
			MqttConnectOptions connOpts = new MqttConnectOptions();
			connOpts.setCleanSession(false);
			sampleClient.connect(connOpts);
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void send(String topic, byte[] message_json) {
		if (flag) {
			init();
			flag = false;
		}
		MqttMessage message = new MqttMessage(message_json);
		message.setQos(qos);
		try {
			sampleClient.publish(topic, message);
//			sampleClient.close();
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
