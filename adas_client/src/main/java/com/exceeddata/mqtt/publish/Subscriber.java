package com.exceeddata.mqtt.publish;

import com.google.protobuf.InvalidProtocolBufferException;
import com.hryt.adas.evaluate.HMIProto;
import hryt.common.HeaderOuterClass;
import hryt.obu.OBU2HMI;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;

public class Subscriber implements MqttCallback {
    private static String URL = "tcp://master:1883";
    private static final String OBU2TSP = "hryttest";
    private static final String HMI2TSP = "hryt/adas/evaluate/hmi2tsp";
    private static final String TSP2HMI = "hryt/adas/evaluate/tsp2hmi";
    private final int qos = 1;
    private String topic = "hryttest";
    private MqttClient client;
    private static Logger logger = Logger.getLogger(Subscriber.class);

    public Subscriber(String uri) throws MqttException, URISyntaxException {
        this(new URI(uri));
        logger.setLevel(Level.INFO);
        String relativelyPath=System.getProperty("user.dir");
        PropertyConfigurator.configure(relativelyPath+ "/src/main/resources/log/log4j.properties");
    }

    public Subscriber(URI uri) throws MqttException {
        String host = String.format("tcp://%s:%d", uri.getHost(), uri.getPort());
        String[] auth = this.getAuth(uri);
        String username = auth[0];
        String password = auth[1];
        String clientId = "MQTT-Java-Example";
        if (!uri.getPath().isEmpty()) {
            this.topic = uri.getPath().substring(1);
        }

        MqttConnectOptions conOpt = new MqttConnectOptions();
        conOpt.setCleanSession(true);
        conOpt.setUserName(username);
        conOpt.setPassword(password.toCharArray());

        this.client = new MqttClient(host, clientId, new MemoryPersistence());
        this.client.setCallback(this);
        this.client.connect(conOpt);

        this.client.subscribe(this.topic, qos);
    }

    private String[] getAuth(URI uri) {
        String a = uri.getAuthority();
        String[] first = a.split("@");
        return first[0].split(":");
    }

    public void sendMessage(String topic, byte[] payload) throws MqttException {
        MqttMessage message = new MqttMessage(payload);
        message.setQos(qos);
        this.client.publish(topic, message); // Blocking publish
    }

    /**
     * @see MqttCallback#connectionLost(Throwable)
     */
    public void connectionLost(Throwable cause) {
        System.out.println("Connection lost because: " + cause);
        System.exit(1);
    }

    /**
     * @see MqttCallback#deliveryComplete(IMqttDeliveryToken)
     */
    public void deliveryComplete(IMqttDeliveryToken token) {
    }

    /**
     * @see MqttCallback#messageArrived(String, MqttMessage)
     */
    public void messageArrived(String topic, MqttMessage message) throws MqttException, InvalidProtocolBufferException {
        final byte[] mess = message.getPayload();
        logger.info(String.format("[%s] %s", topic, new String(mess)));
    }

    public static void main(String[] args) throws MqttException, URISyntaxException {
    }
}
