// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: TSP.proto

package com.hryt.adas.evaluate;

public final class TSPProto {
  private TSPProto() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  public interface TSPAckOrBuilder extends
      // @@protoc_insertion_point(interface_extends:TSPAck)
      com.google.protobuf.MessageOrBuilder {

    /**
     * <pre>
     *时间戳
     * </pre>
     *
     * <code>uint64 time_stamp = 1;</code>
     */
    long getTimeStamp();

    /**
     * <pre>
     *对应消息ID，对应Msg_HMI - msg_cout
     * </pre>
     *
     * <code>uint32 msg_count = 2;</code>
     */
    int getMsgCount();

    /**
     * <pre>
     *对应上传指令：0-initial；1-start；2-end；3-submit evaluation；4-request report
     * </pre>
     *
     * <code>uint32 cmd = 3;</code>
     */
    int getCmd();

    /**
     * <pre>
     *请求反馈0-error；1-正常
     * </pre>
     *
     * <code>uint32 status = 4;</code>
     */
    int getStatus();

    /**
     * <pre>
     *评价按钮激活，0非激活1激活
     * </pre>
     *
     * <code>uint32 subEnable = 5;</code>
     */
    int getSubEnable();

    /**
     * <pre>
     *获取报告按钮激活，0非激活1激活
     * </pre>
     *
     * <code>uint32 ReqEnable = 6;</code>
     */
    int getReqEnable();
  }
  /**
   * Protobuf type {@code TSPAck}
   */
  public  static final class TSPAck extends
      com.google.protobuf.GeneratedMessageV3 implements
      // @@protoc_insertion_point(message_implements:TSPAck)
      TSPAckOrBuilder {
  private static final long serialVersionUID = 0L;
    // Use TSPAck.newBuilder() to construct.
    private TSPAck(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
      super(builder);
    }
    private TSPAck() {
      timeStamp_ = 0L;
      msgCount_ = 0;
      cmd_ = 0;
      status_ = 0;
      subEnable_ = 0;
      reqEnable_ = 0;
    }

    @Override
    public final com.google.protobuf.UnknownFieldSet
    getUnknownFields() {
      return this.unknownFields;
    }
    private TSPAck(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      this();
      if (extensionRegistry == null) {
        throw new NullPointerException();
      }
      int mutable_bitField0_ = 0;
      com.google.protobuf.UnknownFieldSet.Builder unknownFields =
          com.google.protobuf.UnknownFieldSet.newBuilder();
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            default: {
              if (!parseUnknownFieldProto3(
                  input, unknownFields, extensionRegistry, tag)) {
                done = true;
              }
              break;
            }
            case 8: {

              timeStamp_ = input.readUInt64();
              break;
            }
            case 16: {

              msgCount_ = input.readUInt32();
              break;
            }
            case 24: {

              cmd_ = input.readUInt32();
              break;
            }
            case 32: {

              status_ = input.readUInt32();
              break;
            }
            case 40: {

              subEnable_ = input.readUInt32();
              break;
            }
            case 48: {

              reqEnable_ = input.readUInt32();
              break;
            }
          }
        }
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(this);
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(
            e).setUnfinishedMessage(this);
      } finally {
        this.unknownFields = unknownFields.build();
        makeExtensionsImmutable();
      }
    }
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return TSPProto.internal_static_TSPAck_descriptor;
    }

    protected FieldAccessorTable
        internalGetFieldAccessorTable() {
      return TSPProto.internal_static_TSPAck_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              TSPAck.class, Builder.class);
    }

    public static final int TIME_STAMP_FIELD_NUMBER = 1;
    private long timeStamp_;
    /**
     * <pre>
     *时间戳
     * </pre>
     *
     * <code>uint64 time_stamp = 1;</code>
     */
    public long getTimeStamp() {
      return timeStamp_;
    }

    public static final int MSG_COUNT_FIELD_NUMBER = 2;
    private int msgCount_;
    /**
     * <pre>
     *对应消息ID，对应Msg_HMI - msg_cout
     * </pre>
     *
     * <code>uint32 msg_count = 2;</code>
     */
    public int getMsgCount() {
      return msgCount_;
    }

    public static final int CMD_FIELD_NUMBER = 3;
    private int cmd_;
    /**
     * <pre>
     *对应上传指令：0-initial；1-start；2-end；3-submit evaluation；4-request report
     * </pre>
     *
     * <code>uint32 cmd = 3;</code>
     */
    public int getCmd() {
      return cmd_;
    }

    public static final int STATUS_FIELD_NUMBER = 4;
    private int status_;
    /**
     * <pre>
     *请求反馈0-error；1-正常
     * </pre>
     *
     * <code>uint32 status = 4;</code>
     */
    public int getStatus() {
      return status_;
    }

    public static final int SUBENABLE_FIELD_NUMBER = 5;
    private int subEnable_;
    /**
     * <pre>
     *评价按钮激活，0非激活1激活
     * </pre>
     *
     * <code>uint32 subEnable = 5;</code>
     */
    public int getSubEnable() {
      return subEnable_;
    }

    public static final int REQENABLE_FIELD_NUMBER = 6;
    private int reqEnable_;
    /**
     * <pre>
     *获取报告按钮激活，0非激活1激活
     * </pre>
     *
     * <code>uint32 ReqEnable = 6;</code>
     */
    public int getReqEnable() {
      return reqEnable_;
    }

    private byte memoizedIsInitialized = -1;
    public final boolean isInitialized() {
      byte isInitialized = memoizedIsInitialized;
      if (isInitialized == 1) return true;
      if (isInitialized == 0) return false;

      memoizedIsInitialized = 1;
      return true;
    }

    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      if (timeStamp_ != 0L) {
        output.writeUInt64(1, timeStamp_);
      }
      if (msgCount_ != 0) {
        output.writeUInt32(2, msgCount_);
      }
      if (cmd_ != 0) {
        output.writeUInt32(3, cmd_);
      }
      if (status_ != 0) {
        output.writeUInt32(4, status_);
      }
      if (subEnable_ != 0) {
        output.writeUInt32(5, subEnable_);
      }
      if (reqEnable_ != 0) {
        output.writeUInt32(6, reqEnable_);
      }
      unknownFields.writeTo(output);
    }

    public int getSerializedSize() {
      int size = memoizedSize;
      if (size != -1) return size;

      size = 0;
      if (timeStamp_ != 0L) {
        size += com.google.protobuf.CodedOutputStream
          .computeUInt64Size(1, timeStamp_);
      }
      if (msgCount_ != 0) {
        size += com.google.protobuf.CodedOutputStream
          .computeUInt32Size(2, msgCount_);
      }
      if (cmd_ != 0) {
        size += com.google.protobuf.CodedOutputStream
          .computeUInt32Size(3, cmd_);
      }
      if (status_ != 0) {
        size += com.google.protobuf.CodedOutputStream
          .computeUInt32Size(4, status_);
      }
      if (subEnable_ != 0) {
        size += com.google.protobuf.CodedOutputStream
          .computeUInt32Size(5, subEnable_);
      }
      if (reqEnable_ != 0) {
        size += com.google.protobuf.CodedOutputStream
          .computeUInt32Size(6, reqEnable_);
      }
      size += unknownFields.getSerializedSize();
      memoizedSize = size;
      return size;
    }

    @Override
    public boolean equals(final Object obj) {
      if (obj == this) {
       return true;
      }
      if (!(obj instanceof TSPAck)) {
        return super.equals(obj);
      }
      TSPAck other = (TSPAck) obj;

      boolean result = true;
      result = result && (getTimeStamp()
          == other.getTimeStamp());
      result = result && (getMsgCount()
          == other.getMsgCount());
      result = result && (getCmd()
          == other.getCmd());
      result = result && (getStatus()
          == other.getStatus());
      result = result && (getSubEnable()
          == other.getSubEnable());
      result = result && (getReqEnable()
          == other.getReqEnable());
      result = result && unknownFields.equals(other.unknownFields);
      return result;
    }

    @Override
    public int hashCode() {
      if (memoizedHashCode != 0) {
        return memoizedHashCode;
      }
      int hash = 41;
      hash = (19 * hash) + getDescriptor().hashCode();
      hash = (37 * hash) + TIME_STAMP_FIELD_NUMBER;
      hash = (53 * hash) + com.google.protobuf.Internal.hashLong(
          getTimeStamp());
      hash = (37 * hash) + MSG_COUNT_FIELD_NUMBER;
      hash = (53 * hash) + getMsgCount();
      hash = (37 * hash) + CMD_FIELD_NUMBER;
      hash = (53 * hash) + getCmd();
      hash = (37 * hash) + STATUS_FIELD_NUMBER;
      hash = (53 * hash) + getStatus();
      hash = (37 * hash) + SUBENABLE_FIELD_NUMBER;
      hash = (53 * hash) + getSubEnable();
      hash = (37 * hash) + REQENABLE_FIELD_NUMBER;
      hash = (53 * hash) + getReqEnable();
      hash = (29 * hash) + unknownFields.hashCode();
      memoizedHashCode = hash;
      return hash;
    }

    public static TSPAck parseFrom(
        java.nio.ByteBuffer data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static TSPAck parseFrom(
        java.nio.ByteBuffer data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static TSPAck parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static TSPAck parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static TSPAck parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static TSPAck parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static TSPAck parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static TSPAck parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }
    public static TSPAck parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input);
    }
    public static TSPAck parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
    }
    public static TSPAck parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static TSPAck parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }

    public Builder newBuilderForType() { return newBuilder(); }
    public static Builder newBuilder() {
      return DEFAULT_INSTANCE.toBuilder();
    }
    public static Builder newBuilder(TSPAck prototype) {
      return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
    }
    public Builder toBuilder() {
      return this == DEFAULT_INSTANCE
          ? new Builder() : new Builder().mergeFrom(this);
    }

    @Override
    protected Builder newBuilderForType(
        BuilderParent parent) {
      Builder builder = new Builder(parent);
      return builder;
    }
    /**
     * Protobuf type {@code TSPAck}
     */
    public static final class Builder extends
        com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
        // @@protoc_insertion_point(builder_implements:TSPAck)
        TSPAckOrBuilder {
      public static final com.google.protobuf.Descriptors.Descriptor
          getDescriptor() {
        return TSPProto.internal_static_TSPAck_descriptor;
      }

      protected FieldAccessorTable
          internalGetFieldAccessorTable() {
        return TSPProto.internal_static_TSPAck_fieldAccessorTable
            .ensureFieldAccessorsInitialized(
                TSPAck.class, Builder.class);
      }

      // Construct using com.hryt.adas.evaluate.TSPProto.TSPAck.newBuilder()
      private Builder() {
        maybeForceBuilderInitialization();
      }

      private Builder(
          BuilderParent parent) {
        super(parent);
        maybeForceBuilderInitialization();
      }
      private void maybeForceBuilderInitialization() {
        if (com.google.protobuf.GeneratedMessageV3
                .alwaysUseFieldBuilders) {
        }
      }
      public Builder clear() {
        super.clear();
        timeStamp_ = 0L;

        msgCount_ = 0;

        cmd_ = 0;

        status_ = 0;

        subEnable_ = 0;

        reqEnable_ = 0;

        return this;
      }

      public com.google.protobuf.Descriptors.Descriptor
          getDescriptorForType() {
        return TSPProto.internal_static_TSPAck_descriptor;
      }

      public TSPAck getDefaultInstanceForType() {
        return TSPAck.getDefaultInstance();
      }

      public TSPAck build() {
        TSPAck result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return result;
      }

      public TSPAck buildPartial() {
        TSPAck result = new TSPAck(this);
        result.timeStamp_ = timeStamp_;
        result.msgCount_ = msgCount_;
        result.cmd_ = cmd_;
        result.status_ = status_;
        result.subEnable_ = subEnable_;
        result.reqEnable_ = reqEnable_;
        onBuilt();
        return result;
      }

      public Builder clone() {
        return (Builder) super.clone();
      }
      public Builder setField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          Object value) {
        return (Builder) super.setField(field, value);
      }
      public Builder clearField(
          com.google.protobuf.Descriptors.FieldDescriptor field) {
        return (Builder) super.clearField(field);
      }
      public Builder clearOneof(
          com.google.protobuf.Descriptors.OneofDescriptor oneof) {
        return (Builder) super.clearOneof(oneof);
      }
      public Builder setRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          int index, Object value) {
        return (Builder) super.setRepeatedField(field, index, value);
      }
      public Builder addRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          Object value) {
        return (Builder) super.addRepeatedField(field, value);
      }
      public Builder mergeFrom(com.google.protobuf.Message other) {
        if (other instanceof TSPAck) {
          return mergeFrom((TSPAck)other);
        } else {
          super.mergeFrom(other);
          return this;
        }
      }

      public Builder mergeFrom(TSPAck other) {
        if (other == TSPAck.getDefaultInstance()) return this;
        if (other.getTimeStamp() != 0L) {
          setTimeStamp(other.getTimeStamp());
        }
        if (other.getMsgCount() != 0) {
          setMsgCount(other.getMsgCount());
        }
        if (other.getCmd() != 0) {
          setCmd(other.getCmd());
        }
        if (other.getStatus() != 0) {
          setStatus(other.getStatus());
        }
        if (other.getSubEnable() != 0) {
          setSubEnable(other.getSubEnable());
        }
        if (other.getReqEnable() != 0) {
          setReqEnable(other.getReqEnable());
        }
        this.mergeUnknownFields(other.unknownFields);
        onChanged();
        return this;
      }

      public final boolean isInitialized() {
        return true;
      }

      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        TSPAck parsedMessage = null;
        try {
          parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
        } catch (com.google.protobuf.InvalidProtocolBufferException e) {
          parsedMessage = (TSPAck) e.getUnfinishedMessage();
          throw e.unwrapIOException();
        } finally {
          if (parsedMessage != null) {
            mergeFrom(parsedMessage);
          }
        }
        return this;
      }

      private long timeStamp_ ;
      /**
       * <pre>
       *时间戳
       * </pre>
       *
       * <code>uint64 time_stamp = 1;</code>
       */
      public long getTimeStamp() {
        return timeStamp_;
      }
      /**
       * <pre>
       *时间戳
       * </pre>
       *
       * <code>uint64 time_stamp = 1;</code>
       */
      public Builder setTimeStamp(long value) {
        
        timeStamp_ = value;
        onChanged();
        return this;
      }
      /**
       * <pre>
       *时间戳
       * </pre>
       *
       * <code>uint64 time_stamp = 1;</code>
       */
      public Builder clearTimeStamp() {
        
        timeStamp_ = 0L;
        onChanged();
        return this;
      }

      private int msgCount_ ;
      /**
       * <pre>
       *对应消息ID，对应Msg_HMI - msg_cout
       * </pre>
       *
       * <code>uint32 msg_count = 2;</code>
       */
      public int getMsgCount() {
        return msgCount_;
      }
      /**
       * <pre>
       *对应消息ID，对应Msg_HMI - msg_cout
       * </pre>
       *
       * <code>uint32 msg_count = 2;</code>
       */
      public Builder setMsgCount(int value) {
        
        msgCount_ = value;
        onChanged();
        return this;
      }
      /**
       * <pre>
       *对应消息ID，对应Msg_HMI - msg_cout
       * </pre>
       *
       * <code>uint32 msg_count = 2;</code>
       */
      public Builder clearMsgCount() {
        
        msgCount_ = 0;
        onChanged();
        return this;
      }

      private int cmd_ ;
      /**
       * <pre>
       *对应上传指令：0-initial；1-start；2-end；3-submit evaluation；4-request report
       * </pre>
       *
       * <code>uint32 cmd = 3;</code>
       */
      public int getCmd() {
        return cmd_;
      }
      /**
       * <pre>
       *对应上传指令：0-initial；1-start；2-end；3-submit evaluation；4-request report
       * </pre>
       *
       * <code>uint32 cmd = 3;</code>
       */
      public Builder setCmd(int value) {
        
        cmd_ = value;
        onChanged();
        return this;
      }
      /**
       * <pre>
       *对应上传指令：0-initial；1-start；2-end；3-submit evaluation；4-request report
       * </pre>
       *
       * <code>uint32 cmd = 3;</code>
       */
      public Builder clearCmd() {
        
        cmd_ = 0;
        onChanged();
        return this;
      }

      private int status_ ;
      /**
       * <pre>
       *请求反馈0-error；1-正常
       * </pre>
       *
       * <code>uint32 status = 4;</code>
       */
      public int getStatus() {
        return status_;
      }
      /**
       * <pre>
       *请求反馈0-error；1-正常
       * </pre>
       *
       * <code>uint32 status = 4;</code>
       */
      public Builder setStatus(int value) {
        
        status_ = value;
        onChanged();
        return this;
      }
      /**
       * <pre>
       *请求反馈0-error；1-正常
       * </pre>
       *
       * <code>uint32 status = 4;</code>
       */
      public Builder clearStatus() {
        
        status_ = 0;
        onChanged();
        return this;
      }

      private int subEnable_ ;
      /**
       * <pre>
       *评价按钮激活，0非激活1激活
       * </pre>
       *
       * <code>uint32 subEnable = 5;</code>
       */
      public int getSubEnable() {
        return subEnable_;
      }
      /**
       * <pre>
       *评价按钮激活，0非激活1激活
       * </pre>
       *
       * <code>uint32 subEnable = 5;</code>
       */
      public Builder setSubEnable(int value) {
        
        subEnable_ = value;
        onChanged();
        return this;
      }
      /**
       * <pre>
       *评价按钮激活，0非激活1激活
       * </pre>
       *
       * <code>uint32 subEnable = 5;</code>
       */
      public Builder clearSubEnable() {
        
        subEnable_ = 0;
        onChanged();
        return this;
      }

      private int reqEnable_ ;
      /**
       * <pre>
       *获取报告按钮激活，0非激活1激活
       * </pre>
       *
       * <code>uint32 ReqEnable = 6;</code>
       */
      public int getReqEnable() {
        return reqEnable_;
      }
      /**
       * <pre>
       *获取报告按钮激活，0非激活1激活
       * </pre>
       *
       * <code>uint32 ReqEnable = 6;</code>
       */
      public Builder setReqEnable(int value) {
        
        reqEnable_ = value;
        onChanged();
        return this;
      }
      /**
       * <pre>
       *获取报告按钮激活，0非激活1激活
       * </pre>
       *
       * <code>uint32 ReqEnable = 6;</code>
       */
      public Builder clearReqEnable() {
        
        reqEnable_ = 0;
        onChanged();
        return this;
      }
      public final Builder setUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return super.setUnknownFieldsProto3(unknownFields);
      }

      public final Builder mergeUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return super.mergeUnknownFields(unknownFields);
      }


      // @@protoc_insertion_point(builder_scope:TSPAck)
    }

    // @@protoc_insertion_point(class_scope:TSPAck)
    private static final TSPAck DEFAULT_INSTANCE;
    static {
      DEFAULT_INSTANCE = new TSPAck();
    }

    public static TSPAck getDefaultInstance() {
      return DEFAULT_INSTANCE;
    }

    private static final com.google.protobuf.Parser<TSPAck>
        PARSER = new com.google.protobuf.AbstractParser<TSPAck>() {
      public TSPAck parsePartialFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws com.google.protobuf.InvalidProtocolBufferException {
        return new TSPAck(input, extensionRegistry);
      }
    };

    public static com.google.protobuf.Parser<TSPAck> parser() {
      return PARSER;
    }

    @Override
    public com.google.protobuf.Parser<TSPAck> getParserForType() {
      return PARSER;
    }

    public TSPAck getDefaultInstanceForType() {
      return DEFAULT_INSTANCE;
    }

  }

  private static final com.google.protobuf.Descriptors.Descriptor
    internal_static_TSPAck_descriptor;
  private static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_TSPAck_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    String[] descriptorData = {
      "\n\tTSP.proto\"r\n\006TSPAck\022\022\n\ntime_stamp\030\001 \001(" +
      "\004\022\021\n\tmsg_count\030\002 \001(\r\022\013\n\003cmd\030\003 \001(\r\022\016\n\006sta" +
      "tus\030\004 \001(\r\022\021\n\tsubEnable\030\005 \001(\r\022\021\n\tReqEnabl" +
      "e\030\006 \001(\rB\"\n\026com.hryt.adas.evaluateB\010TSPPr" +
      "otob\006proto3"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        }, assigner);
    internal_static_TSPAck_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_TSPAck_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_TSPAck_descriptor,
        new String[] { "TimeStamp", "MsgCount", "Cmd", "Status", "SubEnable", "ReqEnable", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
