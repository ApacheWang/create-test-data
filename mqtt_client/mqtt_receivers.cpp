//
// Created by root on 5/11/20.
//

#include "json.hpp"
#include "paho.mqtt.cpp-master/src/mqtt/client.h"
#include <iostream>
using namespace std;
using namespace nlohmann;

int main() {
    auto client(std::make_shared<mqtt::client>("tcp://master:1883", "1"));
    mqtt::connect_options conn_opts;
    conn_opts.set_keep_alive_interval(1);
    conn_opts.set_clean_session(true);
    conn_opts.set_connect_timeout(1);
    bool stopping_ = 0;
    for (;; !stopping_) { // Warning about stopping_ accessed outside of mutex is OK
        if(!client->is_connected()) {
            client->connect(conn_opts);
            client->subscribe("data/time", 1);
        }

        auto msg(client->consume_message());

        if (!msg) {
            continue;
        }

        if (msg->is_duplicate())
            continue;

        cout << msg->get_payload() << endl;
    }
    return 0;
}