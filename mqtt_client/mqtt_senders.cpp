#include <iostream>
#include <cstdlib>
#include <string>
#include <thread>	// For sleep
#include <atomic>
#include <chrono>
#include <cstring>
#include "mqtt/async_client.h"
#include "utils.cpp"
#include "time.h"
#include "protof/OBU2HMI.pb.h"

using namespace std;
using namespace std::chrono;

const std::string TOPIC = "hryttest";

const std::string DFLT_SERVER_ADDRESS { "tcp://master:1883" };

// The QoS for sending data
const int QOS = 1;

// How often to sample the "data"
const auto SAMPLE_PERIOD = milliseconds(5);

// How much the "data" needs to change before we publish a new value.
const int DELTA_MS = 100;

// How many to buffer while off-line
const int MAX_BUFFERED_MESSAGES = 1200;

// --------------------------------------------------------------------------
// Gets the current time as the number of milliseconds since the epoch:
// like a time_t with ms resolution.

uint64_t timestamp()
{
    auto now = system_clock::now();
    auto tse = now.time_since_epoch();
    auto msTm = duration_cast<milliseconds>(tse);
    return uint64_t(msTm.count());
}

// --------------------------------------------------------------------------

char buf[2056];
char buf1[2056];
char buf2[2056];

double fRand(double fMin, double fMax)
{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

int main(int argc, char* argv[])
{
    string address = (argc > 1) ? string(argv[1]) : DFLT_SERVER_ADDRESS;
    uint64_t trun = (argc > 2) ? stoll(argv[2]) : 0LL;

    cout << "Initializing for server '" << address << "'..." << endl;
    mqtt::async_client cli(address, "", MAX_BUFFERED_MESSAGES);

    // Set callbacks for connected and connection lost.

    cli.set_connected_handler([&cli](const std::string&) {
        std::cout << "*** Connected ("
                  << timestamp() << ") ***" << std::endl;
    });

    cli.set_connection_lost_handler([&cli](const std::string&) {
        std::cout << "*** Connection Lost ("
                  << timestamp() << ") ***" << std::endl;
    });

    mqtt::connect_options connopts;
    mqtt::message willmsg("test/events", "Time publisher disconnected", 1, true);
    mqtt::will_options will(willmsg);
    connopts.set_will(will);
    connopts.set_automatic_reconnect(1, 10);

    std::vector<std::vector<std::string> > data = utils::getData("/root/VehiclePosition.csv", ",");
    std::vector<std::string> feature = data[0];

    json send_data = json::parse("{\n"
                                 "   \"header\":{\n"
                                 "      \"timestamp\":1589106377,\n"
                                 "      \"msg_type\":5\n"
                                 "   },\n"
                                 "   \"speed\":0,\n"
                                 "   \"lng_acc\":0.021,\n"
                                 "   \"lat_acc\":0.024,\n"
                                 "   \"yaw_rate\":0\n"
                                 "}");
    int index = 1;

    hryt::obu::VehiclePosition vehiclePosition;
    hryt::obu::VehicleIMUData vehicleImuData;
    hryt::obu::VehicleMsg vehicleMsg;

    try {
        cout << "Connecting..." << endl;
        cli.connect(connopts)->wait();

        mqtt::topic top(cli, TOPIC, QOS);
        cout << "Publishing data..." << endl;

        while (true) {
            for(int i = 1, size = data.size(); i < size; ++i) {

                hryt::common::Header* header1 = new hryt::common::Header();
                int64_t time1 = stol(data[i][0]);
                header1->set_timestamp(time1);
                header1->set_msg_type(5);
                vehiclePosition.set_allocated_header(header1);
//                double speed = stod(data[i][1]);
                vehiclePosition.set_speed(fRand(20,130));
                vehiclePosition.set_lng_acc(stod(data[i][2]));
                vehiclePosition.set_lat_acc(stod(data[i][3]));
                vehiclePosition.set_yaw_rate(fRand(10,50));
                vehiclePosition.set_id("1");
                vehiclePosition.set_lat(123.123);
                vehiclePosition.set_lng(123.123);
                vehiclePosition.set_head(123.123);


//                cout << vehiclePosition.DebugString() << endl;

                vehiclePosition.SerializeToArray(buf, vehiclePosition.ByteSize());
                top.publish(buf);

//                srand((unsigned)time(NULL));
//                rand();
                hryt::common::Header* header2 = new hryt::common::Header();
                int64_t time2 = stol(data[i][0]);
                header2->set_timestamp(time2);
                header2->set_msg_type(1);

                vehicleImuData.set_allocated_header(header2);
                vehicleImuData.set_acc_x_first(fRand(-6,6));
                vehicleImuData.set_acc_y_first(fRand(-6,6));
                vehicleImuData.set_acc_z_first(fRand(7,9));

                vehicleImuData.set_acc_x_middle(fRand(-6,6));
                vehicleImuData.set_acc_y_middle(fRand(-6,6));
                vehicleImuData.set_acc_z_middle(fRand(7,9));
                vehicleImuData.SerializeToArray(buf2, vehicleImuData.ByteSize());
                top.publish(buf2);

                hryt::common::Header* header3 = new hryt::common::Header();
                int64_t time3 = stol(data[i][0]);
                header3->set_timestamp(time3);
                header3->set_msg_type(6);
                vehicleMsg.set_allocated_header(header3);
                vehicleMsg.set_vehicle_speed(fRand(0,100));
                vehicleMsg.set_steering_angle(fRand(20,60));
                vehicleMsg.set_accel_acut_pos(fRand(6,10));
                vehicleMsg.set_brake_btn(fRand(2,30));
                vehicleMsg.SerializeToArray(buf1, vehicleMsg.ByteSize());
                top.publish(buf1);

                this_thread::sleep_for(std::chrono::milliseconds(100));
            }
        }

        // Disconnect
        cout << "\nDisconnecting..." << endl;
        cli.disconnect()->wait();
        cout << "  ...OK" << endl;
    }
    catch (const mqtt::exception& exc) {
        cerr << exc.what() << endl;
        return 1;
    }

    return 0;
}

