//
// Created by root on 5/11/20.
//

#include "json.hpp"
#include <fstream>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/detail/classification.hpp>
#include <boost/algorithm/string/split.hpp>
using namespace nlohmann;

class utils {
public:
    static json file_to_json(const std::string& filename)
    {
        json j;
        std::ifstream(filename, std::ios::binary) >> j;
        return j;
    }

    static std::vector<std::vector<std::string> > getData(std::string fileName, std::string delimeter = ",") {
        std::ifstream file(fileName);

        std::vector<std::vector<std::string> > dataList;

        std::string line;
// Iterate through each line and split the content using delimeter
        while (getline(file, line)) {
            std::vector<std::string> vec;
            boost::algorithm::split(vec, line, boost::is_any_of(delimeter));
            dataList.push_back(vec);
        }
// Close the File
        file.close();

        return dataList;

    }

};